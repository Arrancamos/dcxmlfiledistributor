//---------------------------------------------------------------------------


#pragma hdrstop

#include "DataModuleDBClass.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TDataModuleDB *DataModuleDB;
//---------------------------------------------------------------------------
__fastcall TDataModuleDB::TDataModuleDB(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TDataModuleDB::FDConnection1AfterConnect(TObject *Sender)
{
	FDQuery1->SQL->Text=
		"IF NOT EXISTS (SELECT * FROM sysobjects where name='FMA_EMF_EmailFrom' and xtype='U')"
		" CREATE TABLE FMA_EMF_EmailFrom("
		"  EMFinID INT IDENTITY,"
		"  EMFchFrom varchar(100) NOT NULL,"
		"  EMFchClave varchar(50) NULL,"
		"  EMFchSubject varchar(255)  NOT NULL,"
		"  EMFdaDateTime smalldatetime  NOT NULL,"
		"  EMFchAttachements varchar(max) NOT NULL)";
}
//---------------------------------------------------------------------------

