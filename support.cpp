//---------------------------------------------------------------------------

#pragma hdrstop

#include "support.h"
#include "System.IOUtils.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)

XMLFileDistributorHaciendaCR::XMLFileDistributorHaciendaCR(String pXMLFile) {
	if (pXMLFile.IsEmpty()) return;
	fLoadXML(pXMLFile);
}

XMLFileDistributorHaciendaCR::~XMLFileDistributorHaciendaCR() {

}

int __fastcall XMLFileDistributorHaciendaCR::fShow() {
	return 10;
}

bool __fastcall XMLFileDistributorHaciendaCR::fLoadXML(String pXMLFile) {
	try {
		xXML = LoadXMLDocument(pXMLFile);
		XMLObject = xXML->DocumentElement;
		return true;
	} catch (...) {
		return false;
	}
}

String __fastcall XMLFileDistributorHaciendaCR::fGetXMLClave() {
	String xAux= "";
	_di_IXMLNode nodeElement= XMLObject->ChildNodes->FindNode("Clave"); //const _di_IXMLNode
	if (nodeElement != NULL) xAux= nodeElement->Text;
	return xAux;
}

String __fastcall XMLFileDistributorHaciendaCR::fCheckXMLHaciendaResponseType() {
	_di_IXMLNode nodeElement= XMLObject->ChildNodes->FindNode("MensajeHacienda"); //const _di_IXMLNode
	if ( (nodeElement != NULL) || (XMLObject->NodeName.CompareIC("MensajeHacienda")==0) ) {
		nodeElement= XMLObject->ChildNodes->FindNode("Clave");
		if (nodeElement != NULL) {
			return nodeElement->Text;
		}
	}
	return "";
}

String __fastcall XMLFileDistributorHaciendaCR::fCheckXMLDocType() {
	String xAuxPath="";
	String xType= "";
	String xClave= fGetXMLClave();
	if (!xClave.IsEmpty() && xClave.Length()>32) {
		xType= xClave.SubString(30, 2);
		switch (StrToInt(xType)) {
		case 1:
			xAuxPath= DIR_INVOICE;
			break;
		case 2:
			xAuxPath= DIR_DEBITNOTE;
			break;
		case 3:
			xAuxPath= DIR_CREDITNOTE;
			break;
		case 4:
			xAuxPath= DIR_TICKET;
			break;
		default:
        	xAuxPath= "";
		}
	}
	return xAuxPath;
}

bool __fastcall XMLFileDistributorHaciendaCR::fCheckXMLHaciendaNITReceptor() {
	_di_IXMLNode nodeElement= XMLObject->ChildNodes->FindNode("NumeroCedulaReceptor");
	if (nodeElement != NULL) {
		if ( nodeElement->Text.CompareIC(NIT_RECEPTOR)==0 )
			return true;
		else
			return false;
	} else
		return false;
}

bool __fastcall XMLFileDistributorHaciendaCR::fCheckXMLCIMANIT() {
	if ( !NIT_RECEPTOR.IsEmpty() && NIT_EMISOR.IsEmpty() ) {
		return fCheckXMLCIMANITReceptor();
	} else if ( !NIT_EMISOR.IsEmpty() && NIT_RECEPTOR.IsEmpty() ) {
		return fCheckXMLCIMANITEmisor();
	} else {
		throw(Exception("En el config, s�lo uno, el NIT_RECEPTOR o NIT_EMISOR debe estar completado, pero no ambos"));
    }
}

//Check if NIT correspond to Client
bool __fastcall XMLFileDistributorHaciendaCR::fCheckXMLCIMANITReceptor() {
	_di_IXMLNode nodeElement= XMLObject->ChildNodes->FindNode("Receptor");
	if (nodeElement != NULL) {
		nodeElement= nodeElement->ChildNodes->FindNode("Identificacion");
		if (nodeElement != NULL) {
			nodeElement= nodeElement->ChildNodes->FindNode("Numero");
			if (nodeElement != NULL && nodeElement->Text.CompareIC(NIT_RECEPTOR)==0) {
				return true;
			} else
				return false;
		} else
			return false;
	} else
		return false;
}

//Check if NIT correspond to Client
bool __fastcall XMLFileDistributorHaciendaCR::fCheckXMLCIMANITEmisor() {
	_di_IXMLNode nodeElement= XMLObject->ChildNodes->FindNode("Emisor");
	if (nodeElement != NULL) {
		nodeElement= nodeElement->ChildNodes->FindNode("Identificacion");
		if (nodeElement != NULL) {
			nodeElement= nodeElement->ChildNodes->FindNode("Numero");
			if (nodeElement != NULL && nodeElement->Text.CompareIC(NIT_EMISOR)==0) {
				return true;
			} else
				return false;
		} else
			return false;
	} else
		return false;
}

bool __fastcall XMLFileDistributorHaciendaCR::fMoveFile(String pSource, String pDestination, bool pDeleteFirst) {
	if ( pDeleteFirst && TFile::Exists(pSource) ) DeleteFileW(pDestination);
	return MoveFile(pSource.c_str(), pDestination.c_str());
}
void __fastcall XMLFileDistributorHaciendaCR::fMoveFileAllPDF_Parts(String pSource, String pDestination, bool pDeleteFirst) {
	fMoveFile(pSource, pDestination, pDeleteFirst);
	for (int i = 1; i < 11; i++) {
		String xSourceName= TPath::GetFileNameWithoutExtension(pSource) + "-" + IntToStr(i)  + ".pdf";
		String xDestinName= TPath::GetFileNameWithoutExtension(pDestination) + "-" + IntToStr(i)  + ".pdf";
		String xSource = ExtractFilePath(pSource) + xSourceName;
		String xDestin = ExtractFilePath(pDestination) + xDestinName;
		if ( pDeleteFirst && TFile::Exists(xSource) ) DeleteFileW(xDestin);
		MoveFile(xSource.c_str(), xDestin.c_str());
	}
}

void __fastcall XMLFileDistributorHaciendaCR::fFileSearch(String pDirOrFile, bool pXMLOnly) {
	String xFileExt= "";
	String xPDFName= "";
	String xPrevPDF= "";
	String xClave= "";
	String xAuxPath= "";
	String xDirOrFile= pDirOrFile;
	bool xXMLOnly= pXMLOnly;

	TSearchRec sr;
	int iAttributes = faAnyFile; //faAnyFile-faDirectory
	iAttributes |= faDirectory;

	xDirOrFile= IncludeTrailingPathDelimiter(xDirOrFile);
	if (FindFirst(xDirOrFile + "*.*", iAttributes, sr) == 0) {
		do {
			if ( (sr.Name==".") || (sr.Name=="..") ) {
				continue;
			} if ( (sr.Attr & faDirectory) == sr.Attr ) {
				fFileSearch(IncludeTrailingPathDelimiter(xDirOrFile) + sr.Name, pXMLOnly);
				continue;
			} else if ( ExtractFileExt(sr.Name).CompareIC(".XML")!=0 ) {
				continue;
			}

			if (!this->fLoadXML(xDirOrFile + sr.Name)) {
				String xAuxName= ChangeFileExt(sr.Name, ".pdf");
				this->fMoveFile(xDirOrFile + sr.Name, this->DIR_REJECTED + sr.Name);
				this->fMoveFileAllPDF_Parts(xDirOrFile + xAuxName, this->DIR_REJECTED + xAuxName);
				continue;
			}

			xClave= this->fCheckXMLHaciendaResponseType();
			if (!xClave.IsEmpty()) {
				if (this->fCheckXMLHaciendaNITReceptor())
					this->fMoveFile(xDirOrFile + sr.Name, this->DIR_RPT_HACIENDA + xClave + "-R.xml");
				else  {
					this->fMoveFile(xDirOrFile + sr.Name, this->DIR_REJECTED + xClave + "-R.xml");
					continue;
				}
			}

			xAuxPath= this->fCheckXMLDocType();
			xClave= this->fGetXMLClave();
			String xName= sr.Name;
			if(int xPos= sr.Name.Pos("-")>0) {
				xName= TPath::GetFileNameWithoutExtension(xName);
				xName= xName.SubString(1, xName.Length()-xPos-1) + ".pdf";
			} else
				xName= ChangeFileExt(xName, ".pdf");

			if (!xClave.IsEmpty() && !xAuxPath.IsEmpty() && this->fCheckXMLCIMANIT()) {
				xPDFName= xAuxPath + xClave + ".pdf";
				if ( FileExists(xDirOrFile + xClave + ".pdf") ) {
					this->fMoveFileAllPDF_Parts( xDirOrFile + xClave + ".pdf", xPDFName );
				} else
					this->fMoveFileAllPDF_Parts( xDirOrFile + xName, xPDFName );
				this->fMoveFile(xDirOrFile + sr.Name, xAuxPath + xClave + ".xml");
			} else if (!xClave.IsEmpty() && !xAuxPath.IsEmpty() && this->fCheckXMLHaciendaNITReceptor() ) {
				gLastPDFName= xAuxPath + xClave + ".pdf";
			} else {
				this->fMoveFile(xDirOrFile + sr.Name, this->DIR_REJECTED + sr.Name);
				this->fMoveFileAllPDF_Parts(xDirOrFile + xName, this->DIR_REJECTED + xName);
			}
		} while (FindNext(sr) == 0);
		FindClose(sr);
	}

	//MOVE ALL NONE TAKEN PDFs TO NOTPAIRED DIR
	iAttributes= faAnyFile-faDirectory;
	if (FindFirst(xDirOrFile + "*.PDF", iAttributes, sr) == 0) {
		do {
			String xName= sr.Name;
			if(int xPos= sr.Name.Pos("-")>0) {
				xName= TPath::GetFileNameWithoutExtension(xName);
				xName= xName.SubString(1, xName.Length()-xPos-1) + ".pdf";
			} else
				xName= ChangeFileExt(xName, ".pdf");

			if (!gLastPDFName.IsEmpty() )
				this->fMoveFileAllPDF_Parts( xDirOrFile + xName, gLastPDFName );
			else
				this->fMoveFile(xDirOrFile + sr.Name, this->DIR_NOT_PAIRED + sr.Name);

		} while (FindNext(sr) == 0);
		FindClose(sr);
	}
	if ( TDirectory::IsEmpty(xDirOrFile) && (xDirOrFile.CompareIC(this->DIR_INBOX)!=0) )
		TDirectory::Delete(xDirOrFile, true);
}
