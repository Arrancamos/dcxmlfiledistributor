//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include <System.Classes.hpp>
#include <Xml.XMLDoc.hpp>
#include <Xml.xmldom.hpp>
#include <Xml.XMLIntf.hpp>
#include <support.h>
//---------------------------------------------------------------------------
USEFORM("DataModuleDBClass.cpp", DataModuleDB); /* TDataModule: File Type */
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
	try
	{
		Application->Initialize();
		//Application->CreateForm(__classid(TForm1), &Form1);
		Application->CreateForm(__classid(TDataModuleDB), &DataModuleDB);
		Application->Run();

		int xParamCnt= ParamCount();
		//if (xParamCnt != 2) return 0;

		String xType= ParamStr(1);
		String xDirOrFile= ExtractShortPathName(ParamStr(2));
		String xTypeToProcess= "";
		bool xXMLOnly= false;

		TStrings *xLog= new TStringList();
		xLog->Add("Param1: "+ xType);
		xLog->Add("Param2: "+ xDirOrFile);
		xLog->SaveToFile("dcXMLFileDistributorHaciendaCR.log", TEncoding::UTF8);

		auto *xXMLFDHCR= new XMLFileDistributorHaciendaCR();

		TStrings *xCfg= new TStringList();
		String xCfgFile= IncludeTrailingPathDelimiter(GetCurrentDir())+"dcXMLFileDistributorHaciendaCR.cfg";
		if (FileExists(xCfgFile)) {
			xCfg->LoadFromFile(xCfgFile);
			xXMLFDHCR->NIT_RECEPTOR= xCfg->Values["NIT_RECEPTOR"];
			xXMLFDHCR->NIT_EMISOR= xCfg->Values["NIT_EMISOR"];
			xXMLFDHCR->DIR_INBOX= IncludeTrailingPathDelimiter(xCfg->Values["INBOX_DIR"]);
			xXMLFDHCR->DIR_REJECTED= IncludeTrailingPathDelimiter(xCfg->Values["REJECTED_DIR"]);
			xXMLFDHCR->DIR_INVOICE= IncludeTrailingPathDelimiter(xCfg->Values["INVOICE_DIR"]);
			xXMLFDHCR->DIR_DEBITNOTE= IncludeTrailingPathDelimiter(xCfg->Values["DEBITNOTE_DIR"]);
			xXMLFDHCR->DIR_CREDITNOTE= IncludeTrailingPathDelimiter(xCfg->Values["CREDITNOTE_DIR"]);
			xXMLFDHCR->DIR_TICKET= IncludeTrailingPathDelimiter(xCfg->Values["TICKET_DIR"]);
			xXMLFDHCR->DIR_RPT_HACIENDA= IncludeTrailingPathDelimiter(xCfg->Values["RPT_HACIENDA_DIR"]);
			xXMLFDHCR->DIR_NOT_PAIRED= IncludeTrailingPathDelimiter(xCfg->Values["NOT_PAIRED_DIR"]);
			xXMLOnly= StrToBoolDef(xCfg->Values["XMLOnly"], false);
			xTypeToProcess= xCfg->Values["DIR_OR_FILE"];
		}
		delete xCfg;

		//log file
		xLog->Add("NIT_RECEPTOR: " + xXMLFDHCR->NIT_RECEPTOR);
		xLog->Add("NIT_EMISOR: " + xXMLFDHCR->NIT_EMISOR);
		xLog->Add("INBOX_DIR: " + xXMLFDHCR->DIR_INBOX);
		xLog->Add("REJECTED_DIR: " + xXMLFDHCR->DIR_REJECTED);
		xLog->Add("INVOICE_DIR: " + xXMLFDHCR->DIR_INVOICE);
		xLog->Add("DEBITNOTE_DIR: " + xXMLFDHCR->DIR_DEBITNOTE);
		xLog->Add("CREDITNOTE_DIR: " + xXMLFDHCR->DIR_CREDITNOTE);
		xLog->Add("TICKET_DIR: " + xXMLFDHCR->DIR_TICKET);
		xLog->Add("RPT_HACIENDA_DIR: " + xXMLFDHCR->DIR_RPT_HACIENDA);
		xLog->Add("NOT_PAIRED_DIR: " + xXMLFDHCR->DIR_NOT_PAIRED);
		xLog->Add("DIR_OR_FILE: " + xTypeToProcess);
		xLog->Add("XMLOnly: " + BoolToStr(xXMLOnly, true));
		xLog->SaveToFile("dcXMLFileDistributorHaciendaCR.log", TEncoding::UTF8);

		xXMLFDHCR->DIR_INBOX= ExtractShortPathName(xXMLFDHCR->DIR_INBOX);
		xXMLFDHCR->DIR_REJECTED= ExtractShortPathName(xXMLFDHCR->DIR_REJECTED);
		xXMLFDHCR->DIR_INVOICE= ExtractShortPathName(xXMLFDHCR->DIR_INVOICE);
		xXMLFDHCR->DIR_DEBITNOTE= ExtractShortPathName(xXMLFDHCR->DIR_DEBITNOTE);
		xXMLFDHCR->DIR_CREDITNOTE= ExtractShortPathName(xXMLFDHCR->DIR_CREDITNOTE);
		xXMLFDHCR->DIR_TICKET= ExtractShortPathName(xXMLFDHCR->DIR_TICKET);
		xXMLFDHCR->DIR_RPT_HACIENDA= ExtractShortPathName(xXMLFDHCR->DIR_RPT_HACIENDA);
		xXMLFDHCR->DIR_NOT_PAIRED= ExtractShortPathName(xXMLFDHCR->DIR_NOT_PAIRED);

		String xFileExt= "";
		String xClave= "";
		String xAuxPath= "";
		String xPDFName= "";

		if (xParamCnt == 0) {
			xDirOrFile= xXMLFDHCR->DIR_INBOX;
			xType= xTypeToProcess;

			xLog->Add("Zero Param given. Monitoring: "+ xDirOrFile);
			xLog->SaveToFile("dcXMLFileDistributorHaciendaCR.log", TEncoding::UTF8);
		}

		if (xType.CompareIC("FILE")==0 && xType.CompareIC(xTypeToProcess)==0) {
			//xDirOrFile= xDirOrFile + "FE50603091800010954060300100001010000000083100000001.xml";

			if (ExtractFileExt(xDirOrFile).CompareIC(".PDF")==0) {
				String xAuxFile= ChangeFileExt(xDirOrFile, ".xml");
				if (!FileExists(xAuxFile)) Sleep(15000); //wait a bit for the XML file
				xDirOrFile= ChangeFileExt(xDirOrFile, ".xml");
			}

			xFileExt= ExtractFileExt(xDirOrFile);
			if ( (xFileExt.CompareIC(".XML")==0) && FileExists(xDirOrFile) ) {
				xXMLFDHCR->fLoadXML(xDirOrFile);
				xClave= xXMLFDHCR->fCheckXMLHaciendaResponseType();
				if (!xClave.IsEmpty()) {
					xXMLFDHCR->fMoveFile(xDirOrFile, xXMLFDHCR->DIR_RPT_HACIENDA + xClave + ".xml");
					return 0;
				}

				xAuxPath= xXMLFDHCR->fCheckXMLDocType();
				xClave= xXMLFDHCR->fGetXMLClave();
				if (!xClave.IsEmpty() && !xAuxPath.IsEmpty() && xXMLFDHCR->fCheckXMLCIMANIT()) {
					xPDFName= xAuxPath + xClave + ".pdf";
					if ( FileExists(ExtractFilePath(xDirOrFile) + xClave + ".pdf") ) {
						xXMLFDHCR->fMoveFile(ExtractFilePath(xDirOrFile) + xClave + ".pdf", xPDFName);
					} else {
						if ( FileExists( ChangeFileExt(xDirOrFile, ".pdf") ) )
							xXMLFDHCR->fMoveFile( ChangeFileExt(xDirOrFile, ".pdf"), xPDFName );
					}
					if ( FileExists(xPDFName) )
						xXMLFDHCR->fMoveFile(xDirOrFile, xAuxPath + xClave + ".xml");
				} else {
					String xAuxName= ChangeFileExt(ExtractFileName(xDirOrFile), ".pdf");
					xXMLFDHCR->fMoveFile(xDirOrFile, xXMLFDHCR->DIR_REJECTED + ExtractFileName(xDirOrFile));
					xXMLFDHCR->fMoveFile(xDirOrFile, xXMLFDHCR->DIR_REJECTED + xAuxName);
				}
			}

		} else if (xType.CompareIC("DIR")==0 && xType.CompareIC(xTypeToProcess)==0) {

			xXMLFDHCR->fFileSearch( xXMLFDHCR->DIR_INBOX, xXMLOnly );

		} else {
			//ShowMessage("Type not recognized.");
		}
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;


}
//---------------------------------------------------------------------------

