//---------------------------------------------------------------------------

#ifndef DataModuleDBClassH
#define DataModuleDBClassH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.Moni.Base.hpp>
#include <FireDAC.Moni.RemoteClient.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Phys.MSSQL.hpp>
#include <FireDAC.Phys.MSSQLDef.hpp>
#include <FireDAC.Phys.ODBCBase.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <FireDAC.VCLUI.Error.hpp>
#include <FireDAC.VCLUI.Wait.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.ConsoleUI.Wait.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Stan.Param.hpp>
//---------------------------------------------------------------------------
class TDataModuleDB : public TDataModule
{
__published:	// IDE-managed Components
	TFDPhysMSSQLDriverLink *FDPhysMSSQLDriverLink1;
	TFDMoniRemoteClientLink *FDMoniRemoteClientLink1;
	TFDGUIxWaitCursor *FDGUIxWaitCursor1;
	TFDGUIxErrorDialog *FDGUIxErrorDialog1;
	TFDConnection *FDConnection1;
	TFDQuery *FDQuery1;
	void __fastcall FDConnection1AfterConnect(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TDataModuleDB(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TDataModuleDB *DataModuleDB;
//---------------------------------------------------------------------------
#endif
