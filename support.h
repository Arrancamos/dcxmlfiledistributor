//---------------------------------------------------------------------------

#ifndef supportH
#define supportH
#include <System.Classes.hpp>
#include <Xml.XMLDoc.hpp>
#include <Xml.xmldom.hpp>
#include <Xml.XMLIntf.hpp>
//---------------------------------------------------------------------------

class XMLFileDistributorHaciendaCR {

public:
	__fastcall XMLFileDistributorHaciendaCR(String pXMLFile="");
	__fastcall ~XMLFileDistributorHaciendaCR();

	String NIT_RECEPTOR="";
	String NIT_EMISOR="";
	String DIR_INBOX="";
	String DIR_SUCCESS="";
	String DIR_REJECTED="";
	String DIR_INVOICE="";
	String DIR_CREDITNOTE="";
	String DIR_DEBITNOTE="";
	String DIR_TICKET="";
	String DIR_RPT_HACIENDA="";
	String DIR_NOT_PAIRED="";
	String gLastPDFName="";
	int __fastcall fShow();
	bool __fastcall fLoadXML(String pXMLFile);

	String __fastcall fCheckXMLHaciendaResponseType();

	String __fastcall fGetXMLClave();
	String __fastcall fCheckXMLDocType();
    bool __fastcall fCheckXMLHaciendaNITReceptor();
	bool __fastcall fCheckXMLCIMANIT();
	bool __fastcall fCheckXMLCIMANITReceptor();
	bool __fastcall fCheckXMLCIMANITEmisor();

	bool __fastcall fMoveFile(String pSource, String pDestination, bool pDeleteFirst=true);
	void __fastcall fMoveFileAllPDF_Parts(String pSource, String pDestination, bool pDeleteFirst=true);
	void __fastcall fFileSearch(String pDirOrFile, bool pXMLOnly);
private:
	_di_IXMLDocument xXML;
	_di_IXMLNode XMLObject;
};
typedef XMLFileDistributorHaciendaCR* TXMLFileDistributorHaciendaCR;

#endif
